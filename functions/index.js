const {onRequest} = require("firebase-functions/v2/https");
// const logger = require("firebase-functions/logger");

const express = require("express");
const mysql = require("mysql2");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
// const port = process.env.PORT || 3001;

// MySQL connection
const db = mysql.createConnection({
  host: "localhost",
  port: 3306,
  user: "root",
  password: "Elpato1997",
  database: "contact_form_db",
});

// Connect to MySQL database
db.connect((err) => {
  if (err) {
    console.error("Error connecting to database:", err);
    return;
  }
  console.log("Connected to MySQL database");
});

// Use body-parser middleware to parse JSON data
app.use(bodyParser.json());

// Enable CORS for cross-origin requests
app.use(cors());

app.get("/hello-world", (req, res) => {
  return res.status(200).json({message: "Hello world"});
});


// GET All
app.get("/api/contact", (req, res) => {
  const sql = "SELECT * FROM contact_form_data";
  db.query(sql, (err, result) => {
    if (err) {
      console.error("Error retrieving data:", err);
      res.status(500).send({message: "Error retrieving data"});
      return;
    }
    res.status(200).send({data: result});
  });
});

// POST Contac-form
app.post("/api/contact", (req, res) => {
  const {name, email, subject, message} = req.body;
  const sql =
    "INSERT INTO contact_form_data (name, email, subject, message) VALUES (?, ?, ?, ?)";
  const values = [name, email, subject, message];

  db.query(sql, values, (err, result) => {
    if (err) {
      console.error("Error adding data:", err);
      res.status(500).send({message: "Error adding data"});
      return;
    }
    res.status(201).send({message: "Data added successfully"});
  });
});

// // Start server Express
// app.listen(port, () => {
//   console.log(`Server listening on port ${port}`);
// });

exports.app = onRequest(app);
